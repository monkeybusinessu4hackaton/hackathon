﻿namespace MonkeyBusiness.Models
{
    public class SpeedInfo
    {
        public int Speed { get; }
        public CycleInfo CycleInfo { get; }

        public SpeedInfo(int speed, CycleInfo cycleInfo)
        {
            Speed = speed;
            CycleInfo = cycleInfo;
        }
    }
}
