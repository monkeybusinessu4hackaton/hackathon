﻿using System;

namespace MonkeyBusiness.Models
{
    public class TrafficLight : ITrafficLight
    {
        private const int CycleDuration = 60 / Constants.Div;

        public int GreenDuration { get; }
        public int YellowDuration { get; }
        public int RedDuration { get; }

        private readonly object _syncObject = new object();

        public DecimalDegrees Position { get; }
        public double SpeedLimit { get; }

        public TrafficLight(int greenLightDuration, int yellowLightDuration, DecimalDegrees position, double speedLimit)
        {
            GreenDuration = greenLightDuration / Constants.Div;
            YellowDuration = yellowLightDuration / Constants.Div;
            RedDuration = CycleDuration - GreenDuration - 2 * YellowDuration;

            Position = position;
            SpeedLimit = speedLimit;
        }

        private CycleInfo CalculateCycle()
        {
            int currentSecond = DateTime.Now.Second % (60 / Constants.Div);

            string current;
            int currentDuration;

            string next;
            int nextDuration;

            if (currentSecond < GreenDuration)
            {
                current = LightState.Green;
                currentDuration = GreenDuration - currentSecond;

                next = LightState.Yellow;
                nextDuration = YellowDuration;
            }
            else if(currentSecond < GreenDuration + YellowDuration)
            {
                current = LightState.Yellow;
                currentDuration = YellowDuration - (currentSecond - GreenDuration);

                next = LightState.Red;
                nextDuration = RedDuration;
            }
            else if (currentSecond < GreenDuration + YellowDuration + RedDuration)
            {
                current = LightState.Red;
                currentDuration = RedDuration - (currentSecond - GreenDuration - YellowDuration);

                next = LightState.Yellow;
                nextDuration = YellowDuration;
            }
            else
            {
                current = LightState.Yellow;
                currentDuration = YellowDuration - (currentSecond - GreenDuration - YellowDuration - RedDuration);

                next = LightState.Green;
                nextDuration = GreenDuration;
            }

            return new CycleInfo(current, currentDuration, next, nextDuration);
        }

        public CycleInfo CheckCycleInfo()
        {
            CycleInfo cycleInfo;

            lock(_syncObject)
            {
                cycleInfo = CalculateCycle();
            }

            return cycleInfo;
        }
    }
}