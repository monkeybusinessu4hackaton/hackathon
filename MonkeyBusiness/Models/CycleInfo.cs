﻿namespace MonkeyBusiness.Models
{
    public class CycleInfo
    {
        public string CurrentLightState { get; private set; }
        public int CurrentLightDuration { get; private set; }

        public string NextLightState { get; private set; }
        public int NextLightDuration { get; private set; }

        public CycleInfo(string current, int currentDuration, string next, int nextDuration)
        {
            CurrentLightState = current;
            CurrentLightDuration = currentDuration;

            NextLightState = next;
            NextLightDuration = nextDuration;
        }
    }
}