﻿namespace MonkeyBusiness.Models
{
    public static class LightState
    {
        public const string Green = "green";
        public const string Yellow = "yellow";
        public const string Red = "red";
    }
}