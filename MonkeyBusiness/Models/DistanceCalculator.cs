﻿using System;

namespace MonkeyBusiness.Models
{
    public class DistanceCalculator : IDistanceCalculator
    {
        private const double EarthRadius = 6378.1;

        public double GetDistance(DecimalDegrees object1, DecimalDegrees object2)
        {
            double dlon = object2.Longitude - object1.Longitude;
            double dlat = object2.Latitude - object1.Latitude;

            double angle = Math.Pow(Math.Sin(dlat / 2), 2) + Math.Cos(object1.Latitude) * Math.Cos(object2.Latitude) * Math.Pow(Math.Sin(dlon / 2), 2);
            double circumference = 2 * Math.Atan2(Math.Sqrt(angle), Math.Sqrt(1 - angle));
            double distance = EarthRadius * circumference;
            
            return distance;
        }
    }
}
