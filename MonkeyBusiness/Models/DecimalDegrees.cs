﻿using System;

namespace MonkeyBusiness.Models
{
    public class DecimalDegrees
    {
        public double Latitude { get; }
        public double Longitude { get; }

        public DecimalDegrees(double latitude, double longitude)
        {
            Latitude = DegreesToRadians(latitude);
            Longitude = DegreesToRadians(longitude);
        }

        private static double DegreesToRadians(double degrees)
        {
            return degrees * Math.PI / 180.0;
        }

    }
}
