﻿using System;

namespace MonkeyBusiness.Models
{
    public class SpeedCalculator : ISpeedCalculator
    {
        private readonly IDistanceCalculator _distanceCalculator;

        public SpeedCalculator(IDistanceCalculator distanceCalculator)
        {
            _distanceCalculator = distanceCalculator;
        }

        public SpeedInfo CalculateSpeed(DecimalDegrees objectPosition, ITrafficLight trafficLight)
        {
            double distance = _distanceCalculator.GetDistance(objectPosition, trafficLight.Position);

            CycleInfo cycleInfo = trafficLight.CheckCycleInfo();

            int speed;

            int durationToNextGreen = GetDurationToNextGreen(cycleInfo, trafficLight);

            if (cycleInfo.CurrentLightState == LightState.Green)
            {
                speed = CalculateSpeed(distance, cycleInfo.CurrentLightDuration);

                if (speed <= trafficLight.SpeedLimit)
                {
                    return new SpeedInfo(speed, cycleInfo);
                }
                
                speed = CalculateSpeed(durationToNextGreen, durationToNextGreen);
            }
            else
            {
                speed = CalculateSpeed(distance, durationToNextGreen);
            }

            if (speed <= trafficLight.SpeedLimit)
            {
                return new SpeedInfo(speed, cycleInfo);
            }

            durationToNextGreen += trafficLight.GreenDuration;

            speed = CalculateSpeed(distance, durationToNextGreen);

            return new SpeedInfo(speed, cycleInfo);
        }

        private static int GetDurationToNextGreen(CycleInfo cycleInfo, ITrafficLight trafficLight)
        {
            if (cycleInfo.CurrentLightState == LightState.Red)
            {
                return 
                    cycleInfo.CurrentLightDuration + 
                    trafficLight.YellowDuration;
            }

            if (cycleInfo.CurrentLightState == LightState.Yellow &&
                cycleInfo.NextLightState == LightState.Green)
            {
                return cycleInfo.CurrentLightDuration;
            }

            if (cycleInfo.CurrentLightState == LightState.Yellow &&
                cycleInfo.NextLightState == LightState.Red)
            {
                return 
                    cycleInfo.CurrentLightDuration + 
                    trafficLight.RedDuration + 
                    trafficLight.YellowDuration;
            }

            if (cycleInfo.CurrentLightState == LightState.Green)
            {
                return 
                    cycleInfo.CurrentLightDuration + 
                    trafficLight.YellowDuration + 
                    trafficLight.RedDuration + 
                    trafficLight.YellowDuration;
            }

            return 0;
        }

        private static int CalculateSpeed(double distance, int lightDuration)
        {
            return Convert.ToInt32(distance / (lightDuration / 3600.0));
        }

        private static int CalculateSpeedForYellowLight(string nextState, double lightDuration, double distance, double speedLimit)
        {
            return nextState == LightState.Red ? 
                CalculateSpeedForRedLight(lightDuration, distance, speedLimit) : 
                CalculateSpeedForGreenLight(lightDuration, distance, speedLimit);
        }

        private static int CalculateSpeedForGreenLight(double lightDuration, double distance, double speedLimit)
        {
            double speed = distance / (lightDuration / 3600.0);

            if (speed < speedLimit)
            {
                speed = speedLimit;
            }

            return Convert.ToInt32(speed);
        }

        private static int CalculateSpeedForRedLight(double lightDuration, double distance, double speedLimit)
        {
            double speed = distance / (lightDuration / 3600.0);

            if (speed > speedLimit)
            {
                speed = speedLimit;
            }

            return Convert.ToInt32(speed);
        }
    }
}
