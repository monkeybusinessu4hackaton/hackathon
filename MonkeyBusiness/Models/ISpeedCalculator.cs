﻿namespace MonkeyBusiness.Models
{
    public interface ISpeedCalculator
    {
        SpeedInfo CalculateSpeed(DecimalDegrees objectPosition, ITrafficLight trafficLight);
    }
}
