﻿namespace MonkeyBusiness.Models
{
    public interface ITrafficLight
    {
        DecimalDegrees Position { get; }
        double SpeedLimit { get; }

        int GreenDuration { get; }
        int YellowDuration { get; }
        int RedDuration { get; }

        CycleInfo CheckCycleInfo();
    }
}
