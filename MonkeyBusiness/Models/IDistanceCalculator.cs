﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MonkeyBusiness.Models
{
    public interface IDistanceCalculator
    {
        double GetDistance(DecimalDegrees object1, DecimalDegrees object2);
    }
}
