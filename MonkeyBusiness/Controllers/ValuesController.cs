﻿using Microsoft.AspNetCore.Mvc;
using MonkeyBusiness.Models;

namespace MonkeyBusiness.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private readonly ISpeedCalculator _speedCalculator;
        private readonly ITrafficLight _trafficLight;

        public ValuesController()
        {
            IDistanceCalculator distanceCalculator = new DistanceCalculator();
            _speedCalculator = new SpeedCalculator(distanceCalculator);
            _trafficLight = new TrafficLight(30, 5, new DecimalDegrees(0, 0), 50);
        }
        
        [HttpGet]
        public IActionResult Get()
        {
            CycleInfo cycleInfo = _trafficLight.CheckCycleInfo();

            return new ObjectResult(cycleInfo);
        }
        
        [HttpGet("{latitude}/{longitude}")]
        public IActionResult Get(double latitude, double longitude)
        {
            DecimalDegrees objectPosition = new DecimalDegrees(latitude, longitude);

            var speed = _speedCalculator.CalculateSpeed(objectPosition, _trafficLight);

            return new ObjectResult(speed);
        }
    }
}
